<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');
Route::get('/admin/data', 'HomeController@anyData')->name('taxiDriversGrid');


Route::get('admin/driver/new','HomeController@getCreateDriver')->name('createDriver');
Route::post('admin/driver/new','HomeController@createDriver')->name('postNewDriver');

Route::post('admin/driver/delete/{id}', 'HomeController@deleteDriver')->name('deleteDriver');

Route::get('/driver', 'DriverController@index')->name('driverHome');