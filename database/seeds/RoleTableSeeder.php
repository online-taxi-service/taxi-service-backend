<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_driver = new Role();
        $role_driver->name = 'driver';
        $role_driver->description = 'Driver user account';
        $role_driver->save();

        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->description = 'Super admin account';
        $role_admin->save();

        $role_user = new Role();
        $role_user->name = 'user';
        $role_user->description = 'Normal user account';
        $role_user->save();
    }
}
